$(document).ready(function(){
    var dummyLoots = [
        {
            id: 'loot-1',
            path: window.location.hostname.includes('localhost') ? 'index.html' : '',
            src: 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/pngegg.png',
            videoGo: 0,
            desc: '',
            html: '<p> You found loot 1!</p>'
        }
    ];
    var uwSettings = {
        widgetSettings:{
            enable: true,
            position: 'left',
            tabIconColor: '#fff',
            tabColor: '#bbb',
            tabColorHover: '#4466ff'
        },
        customAgenda: {
            enable: true,
            settings: {
                enable: true,
                accentColor: '#4466ff',
            }
        },
        treasureHunt: {
            enable: true,
            settings: {
                enable: true,
                blankLootSrc: `https://sg-project-assets.s3.eu-west-2.amazonaws.com/100119/th-badges/blankBadge.svg`,
                hideAllLoots: false,
                passportFloat: false,
                completeModalHtml: `<p>You've found all treasures Yo!!</p>`,
                loots: dummyLoots
            }
        },
        chatGo: {
            enable: true,
            settings: {
                url: 'https://minnit.chat/devteam',
                nickname: 'ben'
            }
        },
        techSupport: {
            enable: true
        },
        discoverGo: {
            enable: true
        }
    };
    uw_init(uwSettings);
});
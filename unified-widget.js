// Unified widget

// load all required CSS
var materialIconsCss = document.createElement('link');
materialIconsCss.id = 'materialIconsCss';
materialIconsCss.rel = 'stylesheet';
materialIconsCss.href = 'https://fonts.googleapis.com/icon?family=Material+Icons+Outlined';
document.head.appendChild(materialIconsCss);

var uw_css = document.createElement('link');
uw_css.id = 'uw_css';
uw_css.rel = 'stylesheet';
window.location.hostname.includes('localhost') 
    ? uw_css.href = 'unified-widget.css'
    : uw_css.href = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/unified-widget/unified-widget.css';
document.head.appendChild(uw_css);

var uw_css_md = document.createElement('link');
uw_css_md.id = 'uw_css_md';
uw_css_md.rel = 'stylesheet';
window.location.hostname.includes('localhost') 
    ? uw_css_md.href = 'unified-widget-md.css'
    : uw_css_md.href = 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/unified-widget/unified-widget-md.css';
document.head.appendChild(uw_css_md);

function addScript(scriptName, src){
    var jsFile = document.createElement('script');
    jsFile.id = scriptName;
    jsFile.src = src;
    document.head.appendChild(jsFile);

    return new Promise((res) =>{
        jsFile.onload = function() {
            res();
        }
    })
}

// create and append unified widget
const uw_create_widget = (settings) => {
    var additionalCssText = `
    .uw_widget_tab{
        color: #fff;
        background-color: ${settings.widgetSettings.tabColor};
    }
    .uw_widget_tab:hover,
    .uw_widget_tab_focused,
    .uw_widget_content_title{
        color: #fff;
        background-color: ${settings.widgetSettings.tabColorHover};
    }`;
    $('head').append($('<style>').text(additionalCssText));

    var widgetPosition = 'left';
    if(typeof settings.widgetSettings.position != 'undefined'){
        widgetPosition = settings.widgetSettings.position
    }

    var body = $('body');
    var widgetContainer = $(`
    <div id='uw_widget' class='uw_widget_container'>
    </div>`).addClass(widgetPosition).appendTo(body);

    var widgetTabsContainer = $(`<div class='uw_widget_tabs_container'>`).appendTo(widgetContainer);
    var widgetContentContainer = $(`<div class='uw_widget_content_container'>`).appendTo(widgetContainer);

    var widgetContentCloseBtn = $(`<div class='uw_widget_close_btn material-icons-outlined'>close</div>`).appendTo(widgetContentContainer).on('click', function(){
        $('.uw_widget_tab').removeClass('uw_widget_tab_focused');
        widgetContainer.removeClass('uw_widget_open');
    });

    // for custom agenda
    if(settings.customAgenda.enable){
        var tab = $(`<div data-uw-tab='uw_tab_ca'><span class="material-icons-outlined">event_available</span></div>`);
        var content = $(`<div id='uw_widget_customagenda'><h3 class='uw_widget_content_title'>${settings.customAgenda.title}</h3>
        <div class='uw_widget_content_content' id='uw_widget_content_ca_container'></div>
        </div>`);
        addService(tab, content);
        addScript('customAgendaJs', 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/custom-agenda/custom-agenda.js')
        .then(() => {
            if(typeof settings.customAgenda.settings != 'undefined' && Object.keys(settings.customAgenda.settings).length > 0){
                var tempSettings = settings.customAgenda.settings;
                tempSettings['id'] = 'uw_widget_content_ca_container';
                tempSettings['itemClasses'] = ['uw_ca_item'];
                tempSettings['addedCallback'] = () => {
                    $('.uw_widget_content').css({display: 'none'});
                    $('#uw_widget_customagenda').css({display: 'block'});
                    $('.uw_widget_container').addClass('uw_widget_open');
                    $('.uw_widget_tab').removeClass('uw_widget_tab_focused');
                    $(`[data-uw-tab='uw_tab_ca']`).addClass('uw_widget_tab_focused');
                };
                ca_init(tempSettings);
            }else{
                ca_init({
                    id: 'uw_widget_content_ca_container',
                    accentColor: settings.widgetSettings.tabColorHover,
                });
            };
        });
    }

    // for discover go
    if(settings.discoverGo.enable){
        var tab = $(`<div><span class="material-icons-outlined">search</span></div>`);
        var content = $(`<div id='uw_widget_discovergo'>
            <h3 class='uw_widget_content_title'>${settings.discoverGo.title}</h3>
            <div class='uw_widget_content_content' id='uw_widget_content_dg_container'></div>
        </div>`);
        addService(tab, content);
        var theScript = settings.discoverGo.devMode ? 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/discover-go-widget/discover-go-widget-dev.js':'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/discover-go-widget/discover-go-widget.js';
        addScript('dg_script', theScript).then(() => {
            dg_init({
                id: 'uw_widget_content_dg_container'
            });
        });
    }

    // for treasure hunt
    if(settings.treasureHunt.enable){
        var tab = $(`<div><span class="material-icons-outlined">star_outline</span></div>`);
        var content = $(`<div id='uw_widget_treasurehunt'>
            <h3 class='uw_widget_content_title'>${settings.treasureHunt.title}</h3>
            <div class='uw_widget_content_content' id='uw_widget_content_th_container'></div>
        </div>`);
        addService(tab, content);
        addScript('th_script', 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/treasure-hunt-prod.js').then(() => {
            var thSettings = settings.treasureHunt.settings;
            thSettings.progressPassportId = 'uw_widget_content_th_container';
            th_init(thSettings);
        })
    }

    // for chat go
    if(settings.chatGo.enable){
        var tab = $(`<div><span class="material-icons-outlined">chat</span></div>`);
        var nickname = settings.chatGo.settings.nickname ? `&nickname=${settings.chatGo.settings.nickname}` : ``;
        var url = settings.chatGo.settings.url+`?embed`+nickname;
        var content = $(`<div id='uw_widget_chatgo'><h3 class='uw_widget_content_title'>${settings.chatGo.title}</h3>
        <iframe class='uw_widget_chatgo_content' src="${url}" allowTransparency="true"></iframe>
        </div>`);
        addService(tab, content);
    }

    // for drift
    if(settings.techSupport.enable){
        var tab = $(`<div id='drift-toggle'><span class="material-icons-outlined">support_agent</span></div>`);
        tab.addClass('uw_widget_tab').appendTo(widgetTabsContainer);
    }

    function addService(tab, content){
        tab.addClass('uw_widget_tab').appendTo(widgetTabsContainer).on('click', function(){
            widgetContainer.addClass('uw_widget_open');
            $('.uw_widget_tab').each(function(){
                $(this).removeClass('uw_widget_tab_focused');
            });
            $(this).addClass('uw_widget_tab_focused');

            $('.uw_widget_content').css({display: 'none'});
            content.css({display: 'block'});
        });

        content.addClass('uw_widget_content').appendTo(widgetContentContainer);
    }
};

var dummyLoots = [
    {
        id: 'loot-1',
        path: window.location.hostname.includes('localhost') ? 'index.html' : '',
        src: 'https://sg-template-assets.s3.eu-west-2.amazonaws.com/common-scripts/treasure-hunt-prod/pngegg.png',
        videoGo: 0,
        desc: '',
        html: '<p> You found loot 1!</p>'
    }
]

window.uw_init = (settings) => {
    var settings_default = {
        widgetSettings: settings.widgetSettings,
        customAgenda: {
            enable: settings.customAgenda.enable,
            title: settings.customAgenda.title || 'My Agenda',
            settings: settings.customAgenda.settings
        },
        treasureHunt: {
            enable: settings.treasureHunt.enable,
            title: settings.treasureHunt.title || 'Treasure Hunt',
            settings: settings.treasureHunt.settings
        },
        chatGo: {
            enable: settings.chatGo.enable,
            title: settings.chatGo.title || 'Chat Go',
            settings: settings.chatGo.settings
        },
        techSupport: {
            enable: settings.techSupport.enable,
        },
        discoverGo: {
            enable: settings.discoverGo.enable,
            title: settings.discoverGo.title || 'Search Videos',
            devMode: settings.discoverGo.devMode || false,
            //enable: false
        }
    }
    uw_create_widget(settings_default);
}